import domain.*;
import org.junit.Test;
import service.UserService;

import javax.xml.ws.RequestWrapper;
import java.util.Arrays;
import java.util.List;

import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static service.UserService.getSortedPermissionsOfUsersWithNameStartingWithA;
import static service.UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS;

public class UserServiceTest {

    private static List<User> users;
    private Person p1, p2, p3, p4;
    private User user1, user2, user3, user4;
    private Role r1, r2, r3;
    private Permission pe1,pe2,pe3,pe4,pe5;
    public UserServiceTest() {

        pe1 = new Permission("Add");
        pe2 = new Permission("Delete");
        pe3 = new Permission("Move");
        pe4 = new Permission("KILL");
        pe5 = new Permission("Invite");


        r1 = new Role("Admin", Arrays.<Permission>asList(pe1, pe2, pe3));
        r2 = new Role("User", Arrays.<Permission>asList(pe3, pe5));
        r3 = new Role("Killer", Arrays.<Permission>asList(pe1, pe2, pe3, pe4));

        String n1 = new String("123698745");
        String n2 = new String("987412365");
        String n3 = new String("587412369");
        String n4 = new String("123654789");
        String n5 = new String("852147963");
        String n6 = new String("789321456");
        String n7 = new String("759362145");

        Address a1 = new Address("Niepodległości", 45, 1, "Gdańsk", "83-152", "Polska");
        Address a2 = new Address("Podkomorzego", 2, 4, "Gdańsk", "83-156", "Polska");
        Address a3 = new Address("Piłsudskiego", 21, 12, "Elbląg", "65-657", "Polska");
        Address a4 = new Address("Piłsudskiego", 32, 10, "Warszawa", "68-548", "Polska");
        Address a5 = new Address("Konwaliowa", 44, 44, "Sosnowiec", "35-228", "Polska");
        Address a6 = new Address("Różana", 18, 5, "Stare", "23-657", "Polska");
        Address a7 = new Address("Morska", 20, 11, "Łyse", "12-485", "Polska");

        p1 = new Person("Olek", "Gorzała", Arrays.<String>asList(n1), Arrays.<Address>asList(a1, a2), r2, 18);
        p2 = new Person("Karol", "Papieros", Arrays.<String>asList(n2, n3), Arrays.<Address>asList(a3), r1, 22);
        p3 = new Person("Kuba", "Sasin", Arrays.<String>asList(n4, n5, n6), Arrays.<Address>asList(a4), r2, 40);
        p4 = new Person("Aleksandra", "Oszuścik", Arrays.<String>asList(n7), Arrays.<Address>asList(a5, a6, a7), r3, 12);

        user1 = new User("alfonsek", "alamakota", p1);
        user2 = new User("PoKeMoNKiLlEr", "pikachu", p4);
        user3 = new User("Desmoksan", "Malboro123", p2);
        user4 = new User("Staruch", "dziadek", p3);

        users = asList(user1, user2, user3, user4);
    }


    @Test
    public void testFindUsersWhoHaveMoreThanOneAddress() {
        assertEquals(asList(user1, user2), UserService.findUsersWhoHaveMoreThanOneAddress(users));
    }

    @Test
    public void testFindOldestPerson() {
        assertEquals(p3, UserService.findOldestPerson(users));
    }

    @Test
    public void testFindUserWithLongestUsername() {
        assertEquals(user2, UserService.findUserWithLongestUsername(users));
    }

    @Test
    public void testprintCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS() {
        printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
    }
    @Test
    public void testgetSortedPermissionsOfUsersWithNameStartingWithA(){
        assertEquals(asList(pe1.getName(),pe2.getName(),pe4.getName(),pe3.getName()),UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
    }
 @Test
    public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(){
        UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
    }
    @Test
    public void testpartitionUserByUnderAndOver18(){
        UserService.partitionUserByUnderAndOver18(users);
    }
    @Test
    public void testgroupUsersByRole(){
        UserService.groupUsersByRole(users);
    }
//
}