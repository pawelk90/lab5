package domain;

/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class User {

    private String userName;
    private String password;
    private Person personDetails;

    public User(String userName, String password, Person personDetails){
        this.userName=userName;
        this.password=password;
        this.personDetails=personDetails;

    }

    public String getUserName() {
        return userName;
    }

    public User setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

    public Person getPersonDetails() {
        return personDetails;
    }

    public User setPersonDetails(Person personDetails) {
        this.personDetails = personDetails;
        return this;
    }
}
