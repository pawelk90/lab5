package service;

import domain.*;


import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.*;

import static java.util.stream.Collectors.toList;


/**
 * Created by Krzysztof Pawlowski on 22/11/15.
 */
public class UserService {



        public static List<User> findUsersWhoHaveMoreThanOneAddress(List<User> users) {
            return users.stream()
                    .filter(user -> user.getPersonDetails().getAddresses().size() > 1)
                    .collect(toList());
        }

        public static Person findOldestPerson(List<User> users) {
            return users.stream()
                    .map(User::getPersonDetails)
                    .max(Comparator.comparing(person->person.getAge()))
                    .get();


        }

        public static User findUserWithLongestUsername(List<User> users) {
            return users.stream()
                    .max(Comparator.comparing(user->user.getUserName().length()))
                    .get();


        }

       public static String getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(List<User> users) {

        String result = users.stream()
                    .map(User::getPersonDetails)
                    .filter(person -> person.getAge() >18)
                    .map(person -> person.getName() + " " +person.getSurname())
                    .collect(Collectors.joining(" , "));
        System.out.println(result);
        return result;
       }

        public static List<String> getSortedPermissionsOfUsersWithNameStartingWithA(List<User> users) {
           return users.stream()
                    .map(User::getPersonDetails)
                    .filter(person -> person.getName().startsWith("A"))
                    .map(user -> user.getRole().getPermissions())
                    .flatMap(permissions ->permissions.stream())
                     .map(permission -> permission.getName())
                    .sorted()
                    .collect(Collectors.toList());

        }

        public static void printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(List<User> users) {
                     users.stream()
                            .map(User::getPersonDetails)
                            .filter(person -> person.getSurname().startsWith("S"))
                            .map(user -> user.getRole().getPermissions())
                             .flatMap(permissions -> permissions.stream())
                            .forEach(permission -> System.out.println(permission.getName().toUpperCase()));
        }

        public static Map<Role, List<User>> groupUsersByRole(List<User> users) {
            Map<Role, List<User>> usersGroupedStream = users.stream()
                    .collect(Collectors.groupingBy(user->user.getPersonDetails().getRole()));
            System.out.println(usersGroupedStream);
            return usersGroupedStream;
        }

        public static Map<Boolean, List<User>> partitionUserByUnderAndOver18(List<User> users) {
            Map<Boolean, List<User>> usersGroupedAge;
            usersGroupedAge = users.stream()
                    .collect(Collectors.partitioningBy(user ->user.getPersonDetails().getAge()>18));
            System.out.println(usersGroupedAge);
            return usersGroupedAge;


        }





}
